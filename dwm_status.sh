#!/bin/bash

# Delimiter character
delim="|"

# Function to generate the status line 
# Font awesome version 4 will need to 
# be installed for icons to be displayed
status_line () {
	# MPD 
 	mpc -f "%artist% - %title%" | grep -v volume: | head -n 1 | awk -v d="$delim" '{printf "%s   %s ",d,$0}'

	# WiFi Connection
	# wpa_supplicant
	wpa_cli list_networks | grep CURRENT | cut -f2 | awk -v d="$delim" '{printf "%s  %s ",d,$0}'
	# Network Manager

	# Ethernet Connection
	cat /sys/class/net/e*/operstate | awk -v d="$delim" '{printf "%s  %s ",d,$0}'

	# ALSA master volume 
	amixer get Master | grep "\[on\]\|\[off\]" | cut -d'[' -f2 | cut -d'%' -f1 | awk -v d="$delim" '{printf "%s %s%% ",d,$0}'
		
	# Battery level
	cat /sys/class/power_supply/BAT0/status /sys/class/power_supply/BAT0/capacity | tr '\n' ' ' | awk -v d="$delim" '{printf "%s %s %s%% ",d,$1,$2}'
	
	# Date and time
	date +"%a %d %b %H:%M" | awk -v d="$delim" '{printf "%s %s",d,$0}'
	
}

# Loop to set the status bar
# Refreshes every 5 seconds &
# Redirects errors to devnull 
while true;
do
	xsetroot -name "$(status_line 2>/dev/null)"
	sleep 5
done
